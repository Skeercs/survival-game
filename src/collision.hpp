#pragma once
#include "components.hpp"
#include <SDL.h>
#include <initializer_list>

constexpr double radian = M_PI / 180;

struct Rect {
    SDL_FPoint ul, ur, dl, dr;
};

void rotatePoints(const SDL_FPoint &center,
                  std::initializer_list<SDL_FPoint *> points,
                  const double angle);

namespace collision {

[[nodiscard]] SDL_Rect makeMoveRectOutline(const SDL_Rect &previous,
                                           const SDL_Rect &current);
[[nodiscard]] SDL_Rect makeRectOutline(const SDL_Rect &rect);
[[nodiscard]] SDL_Rect makeMoveRect(const SDL_Rect &previous,
                                    const SDL_Rect &current);
void makeMoveRect_rotated(const SDL_Rect &previous, const SDL_Rect &current,
                          Rect &rect);
void handleCollision(const SDL_Rect &previous, const CollideC &current,
                     const SDL_Rect &c, CollideC &newCurrent, bool &sameAngle,
                     CollideC &sameAngleCollide);
void handleCollision_rotated(const SDL_Rect &previous, const CollideC &current,
                             const CollideC &c, CollideC &newCurrent,
                             bool &sameAngle, CollideC &sameAngleCollide);
void rotateCollidePoints(const CollideC &collide,
                         std::initializer_list<SDL_FPoint *> points);
[[nodiscard]] bool linesIntersect(const Line *const line1,
                                  const Line *const line2);
[[nodiscard]] bool rectsIntersect(const Rect &f, const Rect &s);
[[nodiscard]] bool checkMoveRectCollision(const SDL_Rect &prev,
                                          const CollideC &curr, Rect &f,
                                          Rect s);
} // namespace collision
