#include "ecs.hpp"
#include "collision.hpp"
#include "velSystem.hpp"
#include <SDL_image.h>
#include <algorithm>
#include <sstream>

template <typename A, typename B, typename C>
std::basic_ostream<A, B> &operator<<(std::basic_ostream<A, B> &os,
                                     const C &cont) {
    for (auto it = begin(cont); it != end(cont); ++it)
        os << (it == begin(cont) ? '"' : ' ') << *it;
    os << "\"\n";
    return os;
}

ECS::ECS(SDL_Renderer *renderer, SDL_Window *window, const std::string basePath,
         const std::string prefPath)
    : renderer(renderer), window(window), basePath(basePath),
      prefPath(prefPath) {
    static const std::string player = "player";
    const std::string path = prefPath + "save/" + player;

    const File *file = loadFile(path.c_str());
    if (file == nullptr) {
        initEntity(player);
        spawnEntity(&player, {-32, 400});
        return;
    }
    SDL_FPoint pos;

    const int fileSize = file->size();
    for (int i = 0; i < fileSize; ++i) {
        const std::vector<std::string> &line = file->at(i);
        if (line.empty())
            continue;

        try {
            if (line.size() < 4)
                throw std::runtime_error("Line must have at least 4 words");

            pos = {std::stof(line[1]), std::stof(line[2])};

            spawnEntity(&line[0], pos, std::stod(line[3]));
        } catch (const std::exception &e) {
            std::cout << "Error spawning player: " << e.what() << "\nError in "
                      << line << "Line " << i + 1 << " in " << path << '\n';
            exit(1);
        }
    }
    delete file;

    const int playerWidth = entities[player].sprite.dst.w,
              playerHeight = entities[player].sprite.dst.h;
    camera.x = pos.x + playerWidth / 2.;
    int windowHeight;
    SDL_GetWindowSize(window, NULL, &windowHeight);
    camera.y = pos.y - windowHeight / 2. + playerHeight / 2.;
}

ECS::~ECS() {
    unloadWorld(true);
    for (auto const &[key, val] : entities) {
        if (val.hasSprite)
            SDL_DestroyTexture(val.sprite.texture);
    }
}

Entity ECS::createEntity() const {
    static Entity id = 0;
    return id++;
}

void ECS::spriteSystem() const {
    int windowW;
    SDL_GetWindowSize(window, &windowW, NULL);
    windowW /= 2;

    for (const SpriteC &s : sprites) {
        const SDL_Rect dst = {int(SDL_floorf(s.dst.x - camera.x + windowW)),
                              int(SDL_floorf(s.dst.y - camera.y)), s.dst.w,
                              s.dst.h};
        SDL_RenderCopyEx(renderer, s.texture, &s.src, &dst, s.angle, &s.center,
                         s.flip);
    }
    // // Debug
    // // colliders
    // SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    // for (const CollideC& c : collides) {
    // 	SDL_FPoint first1 = {float(c.rect.x), float(c.rect.y)},
    // 	           first2 = {first1.x + c.rect.w, first1.y},
    // 	           first3 = {first1.x, first1.y + c.rect.h},
    // 	           first4 = {first2.x, first3.y};
    // 	collision::rotateCollidePoints(c, {&first1, &first2, &first3, &first4});
    // 	SDL_FPoint points[] = {first1, first2, first4, first3, first1};
    // 	for (SDL_FPoint& p : points) {
    // 		p.x = SDL_floorf(p.x - camera.x + windowW),
    // 		p.y = SDL_floorf(p.y - camera.y);
    // 	}
    // 	SDL_RenderDrawLinesF(renderer, points, 5);
    // }
    // SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

    // // sprites
    // SDL_SetRenderDrawColor(renderer, 0, 0, 255, 155);
    // for (const SpriteC& s : sprites) {
    // 	SDL_FPoint first1 = {float(s.dst.x), float(s.dst.y)},
    // 	           first2 = {first1.x + s.dst.w, first1.y},
    // 	           first3 = {first1.x, first1.y + s.dst.h},
    // 	           first4 = {first2.x, first3.y};
    // 	const SDL_FPoint center = {s.dst.x + s.dst.w / 2.f,
    // 	                           s.dst.y + s.dst.h / 2.f};
    // 	rotatePoints(center, {&first1, &first2, &first3, &first4}, s.angle);
    // 	SDL_FPoint points[] = {first1, first2, first4, first3, first1};
    // 	for (SDL_FPoint& p : points) {
    // 		p.x = SDL_floorf(p.x - camera.x + windowW),
    // 		p.y = SDL_floorf(p.y - camera.y);
    // 	}
    // 	SDL_RenderDrawLinesF(renderer, points, 5);
    // }
    // SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
}

SDL_Point ECS::collideSystem(const CollideC &previous,
                             CollideC &current) const {
    CollideC newCurrent = current;

    if (current.rect.y >= previous.rect.y &&
        current.rect.y - previous.rect.y < 1)
        ++current.rect.y;

    const SDL_Rect moveRect =
        collision::makeMoveRectOutline(previous.rect, current.rect);

    std::vector<CollideC> potentialCollides;
    for (const CollideC &c : collides) {
        if (c.entity == current.entity)
            continue;

        const SDL_Rect rect = collision::makeRectOutline(c.rect);
        if (!SDL_HasIntersection(&moveRect, &rect))
            continue;
        potentialCollides.emplace_back(c);
    }

    bool sameAngle = false;
    CollideC sameAngleCollide;
    for (const CollideC &c : potentialCollides) {
        if (!current.angle && !c.angle) {
            collision::handleCollision(previous.rect, current, c.rect,
                                       newCurrent, sameAngle, sameAngleCollide);
            continue;
        }
        collision::handleCollision_rotated(
            previous.rect, current, c, newCurrent, sameAngle, sameAngleCollide);
    }
    current = newCurrent;
    return {current.rect.x - previous.rect.x, current.rect.y - previous.rect.y};
}

void ECS::velSystem(const float deltaTime) {
    std::unordered_map<Entity, SDL_Point> velocities_;
    std::unordered_map<Entity, SDL_FPoint> velocitiesF;
    std::unordered_map<Entity, double> angles;

    const InputC &player = inputs[0];
    SDL_FPoint playerPos, playerVel;
    int playerWidth, playerHeight;

    velSystem::velocitiesFirst(velocities, deltaTime, velocities_, player,
                               playerPos, playerVel);

    std::unordered_set<Entity> canJump;
    for (CollideC &c : collides) {
        auto search = velocities_.find(c.entity);
        if (search == velocities_.end())
            continue;

        CollideC previous = c;
        c.rect.x += search->second.x;
        c.rect.y += search->second.y;
        SDL_Rect currentCopy = c.rect;
        search->second = collideSystem(std::move(previous), c);

        const int pos = SDL_floorf((c.rect.x + chunkSize / 2.) / chunkSize);
        for (int i = -loadRadius; i <= loadRadius; ++i)
            loadWorld(pos + i);

        if (previous.angle != c.angle)
            angles.emplace(c.entity, c.angle);

        if (search->second.x != currentCopy.x - previous.rect.x)
            velocitiesF[c.entity].x =
                currentCopy.x - previous.rect.x - search->second.x;
        else
            velocitiesF[c.entity].x = 0;
        if (search->second.y != currentCopy.y - previous.rect.y) {
            velocitiesF[c.entity].y =
                currentCopy.y - previous.rect.y - search->second.y;
            canJump.emplace(c.entity);
        } else
            velocitiesF[c.entity].y = 0;
    }

    velSystem::inputs(inputs, canJump);

    velSystem::velocities(velocities, velocitiesF);

    velSystem::sprites(sprites, player, playerWidth, playerHeight, velocities_,
                       angles);

    velSystem::velocitiesLast(velocities, deltaTime);

    updateCamera(deltaTime, playerPos, playerVel, playerWidth, playerHeight);
    const int pos = SDL_floorf((camera.x + chunkSize / 2.) / chunkSize);
    for (int i = -loadRadius; i <= loadRadius; ++i)
        loadWorld(pos + i);
}

void ECS::inputSystem(const SDL_Event &event) {
    if (event.key.repeat)
        return;
    int addVelX = 0, addVelY = 0;
    bool jumped = false;
    if (event.type == SDL_KEYDOWN) {
        switch (event.key.keysym.sym) {
        case SDLK_a:
            addVelX = -300;
            break;
        case SDLK_d:
            addVelX = 300;
            break;
        case SDLK_SPACE:
            jumped = true;
            addVelY = -600;
            break;
        }
    } else if (event.type == SDL_KEYUP) {
        switch (event.key.keysym.sym) {
        case SDLK_a:
            addVelX = 300;
            break;
        case SDLK_d:
            addVelX = -300;
            break;
        }
    }
    if (!addVelX && !addVelY)
        return;

    std::unordered_set<Entity> inputs_;

    for (InputC &input : inputs) {
        inputs_.emplace(input.entity);
        if (jumped && input.canJump)
            input.canJump = false;
        else
            addVelY = 0;
    }
    for (VelocityC &v : velocities) {
        auto search = inputs_.find(v.entity);
        if (search == end(inputs_))
            continue;
        v.velX += addVelX;
        v.velY += addVelY;
    }
}

void ECS::spawnEntity(const std::string *entityName, const SDL_FPoint pos,
                      const double angle) {
    if (auto search = entities.find(*entityName); search == end(entities))
        initEntity(*entityName);
    const EntityInfo &info = entities[*entityName];
    const Entity entity = createEntity();
    if (info.hasCollide) {
        CollideC collide = {entity, info.collide.rect};
        collide.rect.x += pos.x;
        collide.rect.y += pos.y;
        collide.angle = angle;
        collides.emplace_back(collide);
    }
    if (info.hasSprite) {
        SpriteC sprite = info.sprite;
        sprite.entity = entity;
        sprite.dst.x += pos.x;
        sprite.dst.y += pos.y;
        sprite.angle = angle;
        sprites.emplace_back(sprite);
    }
    if (info.hasVelocity) {
        const VelocityC velocity{entity, 0, 0, pos.x, pos.y, 900};
        velocities.emplace_back(velocity);
    }
    if (info.hasInput) {
        const InputC input{entity};
        inputs.emplace_back(input);
    }
}

void ECS::initEntity(const std::string fileName) {
    if (auto search = entities.find(fileName); search != end(entities))
        return;

    const File *file = loadFile(basePath + "data/" + fileName);
    if (file == nullptr) {
        std::cout << "File read error: " << SDL_GetError() << std::endl
                  << fileName << std::endl;
        exit(1);
    }
    EntityInfo &info = entities[fileName];

    for (const std::vector<std::string> &line : *file) {
        if (line[0] == "input") {
            info.hasInput = true;
            continue;
        }
        if (line[0] == "velocity") {
            info.hasVelocity = true;
            continue;
        }
        if (line[0] == "sprite") {
            const int lineLength = line.size();

            if (lineLength < 2)
                continue;
            SpriteC sprite;

            const std::string path = basePath + "assets/graphics/" + line[1];
            SDL_Surface *surface = IMG_Load(path.c_str());

            if (surface == NULL) {
                std::cout << "IMG_Load error: " << IMG_GetError() << '\n';
                exit(1);
            }
            sprite.src = sprite.dst = {0, 0, surface->w, surface->h};
            SDL_Texture *texture =
                SDL_CreateTextureFromSurface(renderer, surface);
            SDL_FreeSurface(surface);
            sprite.texture = texture;

            if (lineLength < 3)
                continue;

            try {
                const double scale = std::stod(line[2]);
                sprite.dst.w *= scale;
                sprite.dst.h *= scale;
            } catch (const std::exception &e) {
                std::cout << "Failed to init " << fileName
                          << " scale: " << e.what() << "\nError in " << line;
                exit(1);
            }

            sprite.center = {sprite.dst.w / 2, sprite.dst.h / 2};

            info.hasSprite = true;
            info.sprite = std::move(sprite);
            continue;
        }
        if (line[0] != "collide")
            continue;

        if (line.size() < 3)
            continue;

        info.hasCollide = true;
        if (line.size() < 5)
            continue;

        SDL_Rect &cRect = info.collide.rect;
        const SDL_Rect &sDst = info.sprite.dst;

        try {
            if (!info.hasSprite) {
                cRect = {std::stoi(line[1]), std::stoi(line[2]),
                         std::stoi(line[3]), std::stoi(line[4])};
                continue;
            }
            cRect.x =
                sDst.x + (sDst.w - std::stod(line[1]) * (sDst.w - sDst.x));
            cRect.y =
                sDst.y + (sDst.h - std::stod(line[2]) * (sDst.h - sDst.y));
            cRect.w = std::stod(line[3]) * sDst.w - cRect.x + sDst.x;
            cRect.h = std::stod(line[4]) * sDst.h - cRect.y + sDst.y;

        } catch (const std::exception &e) {
            std::cout << "Failed to init " << fileName
                      << " collide rectangle: " << e.what() << "\nError in "
                      << line;
            exit(1);
        }

        if (cRect.w != sDst.w)
            info.sprite.center.x += (cRect.x - sDst.x) / 2 -
                                    (sDst.x + sDst.w - cRect.x - cRect.w) / 2;
        if (cRect.h != sDst.h)
            info.sprite.center.y += (cRect.y - sDst.y) / 2 -
                                    (sDst.y + sDst.h - cRect.y - cRect.h) / 2;
    }
    if (file != nullptr)
        delete file;
}

void ECS::updateCamera(const float deltaTime, const SDL_FPoint &playerPos,
                       const SDL_FPoint &playerVel, const int playerWidth,
                       const int playerHeight) {
    float displacement = SDL_fabs(playerVel.x) + 100;
    if (playerVel.x < 0)
        displacement *= -1;
    if (!playerVel.x)
        displacement = 0;

    camera.x -= (camera.x - (playerPos.x + playerWidth / 2. + displacement)) *
                deltaTime * 1.4;
    const float screenPos = playerPos.y - camera.y;
    if (screenPos <= 200)
        camera.y -= 200 - screenPos;
    if (screenPos + playerHeight >= 980)
        camera.y += screenPos + playerHeight - 980;
}

void ECS::loadWorld(const int pos) {
    if (auto search = loadedChunks.find(pos); search != end(loadedChunks))
        return;

    const std::string path = prefPath + "save/" + std::to_string(pos);

    const File *file = loadFile(path.c_str());
    if (file == nullptr) {
        generateWorld(pos);
        return;
    }

    const int fileSize = file->size();
    for (int i = 0; i < fileSize; ++i) {
        const std::vector<std::string> line = (*file)[i];
        if (line.empty())
            continue;

        // TODO: terrain

        if (line.size() < 4)
            continue;
        try {
            spawnEntity(&line[0], {std::stof(line[1]), std::stof(line[2])},
                        std::stod(line[3]));
        } catch (const std::exception &e) {
            std::cout << "Error spawning entity in chunk " << pos << ": "
                      << e.what() << "\nLine " << i + 1 << " in " << path
                      << '\n';
        }
    }
    delete file;
    loadedChunks.emplace(pos);
}

void ECS::generateWorld(const int pos) {
    // TODO
}

void ECS::unloadWorld(const bool everything) {
    std::vector<SpriteC> removedSprites;
    std::vector<CollideC> removedCollides;
    std::vector<VelocityC> removedVelocities;
    std::vector<InputC> removedInputs;
    std::unordered_set<int> removedChunks;

    if (!everything) {
        std::unordered_set<Entity> movingEntities;
        std::unordered_set<int> keepChunks;

        for (auto it = begin(velocities); it != end(velocities); ++it)
            movingEntities.emplace((*it).entity);
        for (auto it = begin(inputs); it != end(inputs); ++it)
            movingEntities.emplace((*it).entity);

        for (const CollideC &c : collides) {
            if (movingEntities.find(c.entity) == end(movingEntities))
                continue;
            movingEntities.erase(c.entity);

            keepChunks.emplace(
                SDL_floorf((c.rect.x + chunkSize / 2.) / chunkSize));
        }
        for (const SpriteC &s : sprites) {
            if (movingEntities.find(s.entity) == end(movingEntities))
                continue;
            movingEntities.erase(s.entity);

            keepChunks.emplace(
                SDL_floorf((s.dst.x + chunkSize / 2.) / chunkSize));
        }
        for (const VelocityC &v : velocities) {
            if (movingEntities.find(v.entity) == end(movingEntities))
                continue;
            movingEntities.erase(v.entity);

            keepChunks.emplace(
                SDL_floorf((v.posX + chunkSize / 2.) / chunkSize));
        }

        const int pos = SDL_floorf((camera.x + chunkSize / 2.) / chunkSize),
                  min = (pos - loadRadius) * chunkSize - chunkSize / 2.,
                  max = (pos + loadRadius + 1) * chunkSize - chunkSize / 2.;

        std::unordered_set<Entity> removeEntities, keepEntities;
        for (auto it = begin(collides); it != end(collides); ++it) {
            SDL_FPoint ul = {float((*it).rect.x), float((*it).rect.y)},
                       ur = {ul.x + (*it).rect.w - 1, ul.y},
                       dl = {ul.x, ul.y + (*it).rect.h - 1}, dr = {ur.x, dl.y};
            rotatePoints(ul, {&ul, &ur, &dl, &dr}, (*it).angle);

            const int chunk = SDL_floorf(ul.x / chunkSize + .5);

            if (keepChunks.find(chunk) != end(keepChunks) ||
                (min <= ul.x && ul.x < max)) {
                keepEntities.emplace((*it).entity);
                continue;
            }
            removeEntities.emplace((*it).entity);
        }
        for (auto it = begin(sprites); it != end(sprites);) {
            const int chunk = SDL_floorf((*it).dst.x / float(chunkSize) + .5);

            if (removeEntities.find((*it).entity) == end(removeEntities) &&
                ((keepChunks.find(chunk) != end(keepChunks) ||
                  (min <= (*it).dst.x && (*it).dst.x < max)) ||
                 keepEntities.find((*it).entity) != end(keepEntities))) {
                keepEntities.emplace((*it).entity);
                ++it;
                continue;
            }
            removedSprites.emplace_back(*it);
            removedChunks.emplace(chunk);
            removeEntities.emplace((*it).entity);
            it = sprites.erase(it);
        }
        for (auto it = begin(collides); it != end(collides);) {
            const int chunk = SDL_floorf((*it).rect.x / float(chunkSize) + .5);

            if (removeEntities.find((*it).entity) == end(removeEntities) &&
                (keepChunks.find(chunk) != end(keepChunks) ||
                 keepEntities.find((*it).entity) != end(keepEntities))) {
                keepEntities.emplace((*it).entity);
                ++it;
                continue;
            }
            removedCollides.emplace_back(*it);
            removedChunks.emplace(chunk);
            removeEntities.emplace((*it).entity);
            it = collides.erase(it);
        }
    } else {
        for (auto it = begin(sprites); it != end(sprites);
             it = sprites.erase(it)) {
            const int chunk = SDL_floorf((*it).dst.x / float(chunkSize) + .5);
            removedChunks.emplace(chunk);

            removedSprites.emplace_back(*it);
        }
        for (auto it = begin(collides); it != end(collides);
             it = collides.erase(it)) {
            const int chunk = SDL_floorf((*it).rect.x / float(chunkSize) + .5);
            removedChunks.emplace(chunk);

            removedCollides.emplace_back(*it);
        }
        for (auto it = begin(velocities); it != end(velocities);
             it = velocities.erase(it))
            removedVelocities.emplace_back(*it);
        for (auto it = begin(inputs); it != end(inputs); it = inputs.erase(it))
            removedInputs.emplace_back(*it);
    }

    static const std::string savePath = prefPath + "save/";
    std::unordered_map<int, SDL_RWops *> files;

    for (const auto &chunk : removedChunks) {
        loadedChunks.erase(chunk);

        if (files.find(chunk) == end(files))
            files[chunk] =
                SDL_RWFromFile((savePath + std::to_string(chunk)).c_str(), "w");
        if (files[chunk] == NULL) {
            std::cout << "Couldn't write file: " << SDL_GetError();
            exit(1);
        }
        // TODO: save terrain
    }

    std::unordered_map<Entity, EntityInfo> removedEntities;
    for (const auto &s : removedSprites) {
        removedEntities[s.entity].hasSprite = true;
        removedEntities[s.entity].sprite = s;
    }
    for (const auto &c : removedCollides) {
        removedEntities[c.entity].hasCollide = true;
        removedEntities[c.entity].collide = c;
    }
    for (const auto &v : removedVelocities)
        removedEntities[v.entity].hasVelocity = true;
    for (const auto &i : removedInputs)
        removedEntities[i.entity].hasInput = true;

    SDL_RWops *playerFile = nullptr;

    for (auto const &entity : removedEntities) {
        const EntityInfo &info = entity.second;

        const std::string name =
            std::find_if(begin(entities), end(entities),
                         [&info](const auto &e) { return e.second == info; })
                ->first;

        if (info.hasInput) {
            playerFile = SDL_RWFromFile((savePath + "player").c_str(), "w");
            if (playerFile == NULL) {
                std::cout << "Couldn't write file: " << SDL_GetError();
                exit(1);
            }
            const std::string string =
                name + ' ' +
                (info.hasSprite
                     ? std::to_string(info.sprite.dst.x) + ' ' +
                           std::to_string(info.sprite.dst.y) + ' ' +
                           std::to_string(info.sprite.angle)
                     : std::to_string(info.collide.rect.x) + ' ' +
                           std::to_string(info.collide.rect.y) + ' ' +
                           std::to_string(info.collide.angle)) +
                '\n';
            SDL_RWwrite(playerFile, string.c_str(), sizeof(char),
                        string.length());
            continue;
        }
        const int chunk = SDL_floorf(
            ((info.hasSprite ? info.sprite.dst.x : info.collide.rect.x) +
             chunkSize / 2.) /
            float(chunkSize));

        if (files.find(chunk) == end(files))
            files[chunk] =
                SDL_RWFromFile((savePath + std::to_string(chunk)).c_str(), "w");
        if (files[chunk] == NULL) {
            std::cout << "Couldn't write file: " << SDL_GetError();
            exit(1);
        }
        const std::string string =
            name + ' ' +
            (info.hasSprite ? std::to_string(info.sprite.dst.x) + ' ' +
                                  std::to_string(info.sprite.dst.y) + ' ' +
                                  std::to_string(info.sprite.angle)
                            : std::to_string(info.collide.rect.x) + ' ' +
                                  std::to_string(info.collide.rect.y) + ' ' +
                                  std::to_string(info.collide.angle)) +
            '\n';
        SDL_RWwrite(files[chunk], string.c_str(), sizeof(char),
                    string.length());
    }
    if (playerFile != nullptr)
        SDL_RWclose(playerFile);
    for (const auto &f : files)
        SDL_RWclose(f.second);
}

[[nodiscard]] const File *loadFile(const std::string &fileName) {
    SDL_RWops *read = SDL_RWFromFile(fileName.c_str(), "r");
    if (read == NULL)
        return nullptr;

    int fileSize = SDL_RWsize(read);
    char *buffer = new char[fileSize];
    SDL_RWread(read, buffer, sizeof(char), fileSize);
    SDL_RWclose(read);
    std::stringstream string(buffer);
    delete[] buffer;

    File *file = new File;
    std::string word, line;
    for (int i = 0; std::getline(string, line); ++i) {
        file->emplace_back(std::vector<std::string>());
        std::stringstream ss(line);
        for (int j = 0; ss >> word; ++j)
            file->back().emplace_back(word);
    }
    return file;
}
