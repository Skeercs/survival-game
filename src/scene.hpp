#pragma once
#include <SDL.h>

class Scene {
  public:
    virtual void draw() const = 0;
    virtual int handleEvent(const SDL_Event &event) = 0;
    virtual ~Scene(){};
};
