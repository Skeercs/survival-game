#pragma once
#include <SDL.h>

using Entity = Uint32;

struct Component {
    Entity entity;
};

struct SpriteC : Component {
    SDL_Rect src;
    SDL_Rect dst;
    SDL_Texture *texture;
    SDL_Point center;
    double angle = 0; // degrees
    SDL_RendererFlip flip = SDL_FLIP_NONE;
};

struct VelocityC : Component {
    float velX = 0;
    float velY = 0;
    float posX;
    float posY;
    float accelerationY = 0;
};

struct InputC : Component {
    bool canJump = false;
};

struct CollideC : Component {
    SDL_Rect rect;
    double angle = 0;
};

struct EntityInfo {
    bool hasSprite;
    SpriteC sprite;
    bool hasCollide = false;
    CollideC collide;
    bool hasInput = false;
    bool hasVelocity = false;
    bool operator==(const EntityInfo &other) const {
        if (hasInput != other.hasInput || hasVelocity != other.hasVelocity ||
            hasCollide != other.hasCollide || hasSprite != other.hasSprite ||
            (hasSprite && sprite.texture != other.sprite.texture))
            return false;
        return true;
    }
    bool operator!=(const EntityInfo &other) const {
        return !operator==(other);
    }
};

struct Line {
    const SDL_FPoint &first, &second;
};
