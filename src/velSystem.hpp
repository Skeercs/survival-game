#pragma once
#include "components.hpp"
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace velSystem {

void velocitiesFirst(std::vector<VelocityC> &velocities, const float deltaTime,
                     std::unordered_map<Entity, SDL_Point> &velocities_,
                     const InputC &player, SDL_FPoint &playerPos,
                     SDL_FPoint &playerVel);
void inputs(std::vector<InputC> &inputs,
            const std::unordered_set<Entity> &canJump);
void velocities(std::vector<VelocityC> &velocities,
                const std::unordered_map<Entity, SDL_FPoint> &velocitiesF);
void sprites(std::vector<SpriteC> &sprites, const InputC &player,
             int &playerWidth, int &playerHeight,
             const std::unordered_map<Entity, SDL_Point> &velocities_,
             const std::unordered_map<Entity, double> &angles);
void velocitiesLast(std::vector<VelocityC> &velocities, const float deltaTime);

} // namespace velSystem
