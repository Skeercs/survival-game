#pragma once
#include "components.hpp"
#include <SDL.h>
#include <iostream>
#include <map>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using Entity = Uint32;
using File = std::vector<std::vector<std::string>>;

class ECS {
  public:
    ECS(SDL_Renderer *renderer, SDL_Window *window, const std::string basePath,
        const std::string prefPath);
    ~ECS();
    void unloadWorld(const bool everything = false);

    void initEntity(const std::string fileName);
    void spawnEntity(const std::string *entity, const SDL_FPoint pos,
                     const double angle = 0);
    Entity createEntity() const;

    void spriteSystem() const;
    void velSystem(const float deltaTime);
    void inputSystem(const SDL_Event &event);
    SDL_Point collideSystem(const CollideC &c, CollideC &current) const;

  private:
    SDL_FPoint camera = {0, 0};
    static constexpr unsigned short loadRadius = 1;
    static constexpr int chunkSize = 5940;

    std::map<const std::string, EntityInfo> entities;
    std::vector<SpriteC> sprites;
    std::vector<VelocityC> velocities;
    std::vector<InputC> inputs;
    std::vector<CollideC> collides;

    void updateCamera(const float deltaTime, const SDL_FPoint &playerPos,
                      const SDL_FPoint &playerVel, const int playerWidth,
                      const int playerHeight);

    std::unordered_set<int> loadedChunks;
    void loadWorld(const int pos);
    void generateWorld(const int pos);

    SDL_Renderer *renderer;
    SDL_Window *window;
    const std::string basePath, prefPath;
};

[[nodiscard]] const File *loadFile(const std::string &fileName);
