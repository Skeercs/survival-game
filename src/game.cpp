#include "game.hpp"

Game::Game(SDL_Renderer *renderer, SDL_Window *window,
           const std::string &basePath, const std::string &prefPath)
    : ecs(renderer, window, basePath, prefPath), renderer(renderer),
      window(window) {}

Game::~Game() {}

void Game::draw() const {
    SDL_RenderClear(renderer);
    ecs.spriteSystem();
}

int Game::handleEvent(const SDL_Event &event) {
    ecs.inputSystem(event);
    return 0;
}

void Game::update(const float dt, const int time) {
    ecs.velSystem(dt);
    static int prevTime = 0;
    if (time - 4 > prevTime) {
        prevTime = time;
        ecs.unloadWorld();
    }
}
