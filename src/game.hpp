#pragma once
#include "ecs.hpp"
#include "scene.hpp"

class Game : public Scene {
  public:
    Game(SDL_Renderer *renderer, SDL_Window *window,
         const std::string &basePath, const std::string &prefPath);
    ~Game();
    void draw() const;
    int handleEvent(const SDL_Event &event);
    void update(const float dt, const int time);

  private:
    ECS ecs;
    SDL_Renderer *renderer;
    SDL_Window *window;
};
