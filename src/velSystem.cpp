#include "velSystem.hpp"

namespace velSystem {

void velocitiesFirst(std::vector<VelocityC> &velocities, const float deltaTime,
                     std::unordered_map<Entity, SDL_Point> &velocities_,
                     const InputC &player, SDL_FPoint &playerPos,
                     SDL_FPoint &playerVel) {
    for (VelocityC &v : velocities) {
        const float addVel = v.accelerationY * deltaTime;
        v.velY += addVel / (v.velY < 0 ? 2 : 1);
        const int previousX = v.posX, previousY = v.posY;
        v.posX += v.velX * deltaTime;
        v.posY += v.velY * deltaTime;
        v.velY += addVel / (v.velY < 0 ? 2 : 1);

        const SDL_Point add = {int(v.posX) - previousX,
                               int(v.posY) - previousY};
        velocities_.emplace(v.entity, add);
        if (v.entity != player.entity)
            continue;
        playerPos = {v.posX, v.posY};
        playerVel = {v.velX, v.velY};
    }
}

void inputs(std::vector<InputC> &inputs,
            const std::unordered_set<Entity> &canJump) {
    for (InputC &input : inputs) {
        if (canJump.find(input.entity) != end(canJump))
            input.canJump = true;
    }
}

void velocities(std::vector<VelocityC> &velocities,
                const std::unordered_map<Entity, SDL_FPoint> &velocitiesF) {
    for (VelocityC &vel : velocities) {
        auto search = velocitiesF.find(vel.entity);
        if (search == end(velocitiesF))
            continue;

        if (search->second.x)
            vel.posX = int(vel.posX - search->second.x);
        if (search->second.y) {
            vel.posY = int(vel.posY - search->second.y);
            vel.velY = 0;
        }
    }
}

void sprites(std::vector<SpriteC> &sprites, const InputC &player,
             int &playerWidth, int &playerHeight,
             const std::unordered_map<Entity, SDL_Point> &velocities_,
             const std::unordered_map<Entity, double> &angles) {
    for (SpriteC &sprite : sprites) {
        if (sprite.entity == player.entity) {
            playerWidth = sprite.dst.w;
            playerHeight = sprite.dst.h;
        }
        auto search = velocities_.find(sprite.entity);
        if (search == end(velocities_))
            continue;
        auto angle = angles.find(sprite.entity);
        if (angle != end(angles))
            sprite.angle = angle->second;

        sprite.dst.x += search->second.x;
        sprite.dst.y += search->second.y;
    }
}

void velocitiesLast(std::vector<VelocityC> &velocities, const float deltaTime) {
    for (VelocityC &v : velocities)
        v.velY += v.accelerationY * deltaTime * .5 * (v.velY >= 0 ? 2 : 1);
}

} // namespace velSystem
