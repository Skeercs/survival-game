#include "collision.hpp"

void rotatePoints(const SDL_FPoint &center,
                  std::initializer_list<SDL_FPoint *> points,
                  const double angle) {
    if (!angle)
        return;
    const double radians = angle * radian;
    const double cos = std::cos(-radians), sin = std::sin(-radians);

    for (SDL_FPoint *p : points) {
        const float x = p->x - center.x, y = center.y - p->y;
        p->x = center.x + (x * cos - y * sin);
        p->y = center.y - (y * cos + x * sin);
    }
}

namespace collision {

SDL_Rect makeMoveRectOutline(const SDL_Rect &previous,
                             const SDL_Rect &current) {
    const int dist = sqrt((current.w / 2.) * (current.w / 2.) +
                          (current.h / 2.) * (current.h / 2.));

    SDL_Rect rect = current;

    if (current.x < previous.x) {
        rect.x = current.x + current.w / 2 - dist;
        rect.w = previous.x - current.x + dist * 2;
    } else {
        rect.x = previous.x + previous.w / 2 - dist;
        rect.w = current.x - previous.x + dist * 2;
    }
    if (current.y < previous.y) {
        rect.y = current.y + current.h / 2 - dist;
        rect.h = previous.y - current.y + dist * 2;
    } else {
        rect.y = previous.y + previous.h / 2 - dist;
        rect.h = current.y - previous.y + dist * 2;
    }

    return rect;
}

SDL_Rect makeRectOutline(const SDL_Rect &rect) {
    const int dist =
        sqrt((rect.w / 2.) * (rect.w / 2.) + (rect.h / 2.) * (rect.h / 2.));
    const SDL_Rect outline = {rect.x + rect.w / 2 - dist,
                              rect.y + rect.h / 2 - dist, dist * 2, dist * 2};
    return outline;
}

SDL_Rect makeMoveRect(const SDL_Rect &previous, const SDL_Rect &current) {
    SDL_Rect rect;
    if (current.x < previous.x) {
        rect.x = current.x;
        rect.w = previous.w + previous.x - current.x;
    } else {
        rect.x = previous.x;
        rect.w = current.w + current.x - previous.x;
    }
    if (current.y < previous.y) {
        rect.y = current.y;
        rect.h = previous.h + previous.y - current.y;
    } else {
        rect.y = previous.y;
        rect.h = current.h + current.y - previous.y;
    }
    return rect;
}

void makeMoveRect_rotated(const SDL_Rect &previous, const SDL_Rect &current,
                          Rect &rect) {
    if (current.x > previous.x) {
        rect.ul.x -= current.x - previous.x;
        rect.dl.x -= current.x - previous.x;
    } else {
        rect.ur.x -= current.x - previous.x;
        rect.dr.x -= current.x - previous.x;
    }
    if (current.y > previous.y) {
        rect.ul.y -= current.y - previous.y;
        rect.ur.y -= current.y - previous.y;
    } else {
        rect.dl.y -= current.y - previous.y;
        rect.dr.y -= current.y - previous.y;
    }
}

void handleCollision(const SDL_Rect &previous, const CollideC &current,
                     const SDL_Rect &c, CollideC &newCurrent, bool &sameAngle,
                     CollideC &sameAngleCollide) {
    SDL_Rect moveRect = makeMoveRect(previous, current.rect);
    if (!SDL_HasIntersection(&c, &moveRect))
        return;

    if (previous.y >= c.y + c.h) {
        newCurrent.rect.y = c.y + c.h;
    } else if (current.rect.y >= previous.y ||
               (current.rect.y < previous.y && previous.y >= c.y + c.h)) {
        newCurrent.rect.y = c.y - current.rect.h;
    } else if (previous.x >= c.x + c.w) {
        newCurrent.rect.x = c.x + c.w;
    } else if (previous.x >= current.rect.x) {
        newCurrent.rect.x = c.x - current.rect.w;
    }

    if (current.angle == newCurrent.angle) {
        sameAngle = true;
        sameAngleCollide = newCurrent;
    } else if (sameAngle)
        newCurrent = sameAngleCollide;
}

void handleCollision_rotated(const SDL_Rect &previous, const CollideC &current,
                             const CollideC &c, CollideC &newCurrent,
                             bool &sameAngle, CollideC &sameAngleCollide) {
    Rect f;
    f.ul = {float(c.rect.x), float(c.rect.y)},
    f.ur = {f.ul.x + c.rect.w, f.ul.y};
    f.dl = {f.ul.x, f.ul.y + c.rect.h}, f.dr = {f.ur.x, f.dl.y};
    Rect s;
    s.ul = {float(current.rect.x), float(current.rect.y)},
    s.ur = {s.ul.x + current.rect.w, s.ul.y},
    s.dl = {s.ul.x, s.ul.y + current.rect.h}, s.dr = {s.ur.x, s.dl.y};
    rotateCollidePoints(c, {&f.ul, &f.ur, &f.dl, &f.dr});
    rotateCollidePoints(current, {&s.ul, &s.ur, &s.dl, &s.dr});

    if (!checkMoveRectCollision(previous, current, f, s))
        return;

    const SDL_FPoint prevPos = {(s.dl.x + s.ur.x - current.rect.w) / 2,
                                (s.dl.y + s.ur.y - current.rect.h) / 2};

    const SDL_FPoint center = {(s.dl.x + s.dr.x) / 2, (s.dl.y + s.dr.y) / 2};
    rotatePoints(center, {&s.dl, &s.ur}, c.angle - current.angle);

    const float prev = s.dl.y;
    const float slope = (f.ur.y - f.ul.y) / (f.ur.x - f.ul.x);
    if (slope > 0)
        s.dl.y = f.ur.y + slope * (s.dl.x - f.ur.x);
    else
        s.dl.y = f.ul.y + slope * (s.dl.x - f.ul.x);
    if (current.rect.y <= previous.y && previous.y < c.rect.y + c.rect.h &&
        s.dl.y > prev)
        s.dl.y = prev;

    SDL_FPoint currPos = {(s.dl.x + s.ur.x - current.rect.w) / 2,
                          (s.dl.y + s.ur.y - current.rect.h) / 2};
    newCurrent.rect.x = current.rect.x + currPos.x - prevPos.x;
    newCurrent.rect.y = current.rect.y + currPos.y - prevPos.y;
    newCurrent.angle = c.angle;
    if (current.angle == c.angle) {
        sameAngle = true;
        sameAngleCollide = newCurrent;
        return;
    }
    if (sameAngle)
        newCurrent = sameAngleCollide;
}

void rotateCollidePoints(const CollideC &collide,
                         std::initializer_list<SDL_FPoint *> points) {
    const SDL_FPoint center = {collide.rect.x + collide.rect.w / 2.f,
                               collide.rect.y + collide.rect.h / 2.f};
    rotatePoints(center, std::move(points), collide.angle);
}

bool linesIntersect(const Line *const a, const Line *const b) {
    const float div = (b->second.y - b->first.y) * (a->second.x - a->first.x) -
                      (b->second.x - b->first.x) * (a->second.y - a->first.y),
                mulY = a->first.y - b->first.y, mulX = a->first.x - b->first.x;
    const float uA = ((b->second.x - b->first.x) * mulY -
                      (b->second.y - b->first.y) * mulX) /
                     div,
                uB = ((a->second.x - a->first.x) * mulY -
                      (a->second.y - a->first.y) * mulX) /
                     div;
    if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1)
        return true;
    return false;
}

bool rectsIntersect(const Rect &f, const Rect &s) {
    const Line f1 = {f.ul, f.ur}, f2 = {f.ur, f.dr}, f3 = {f.dr, f.dl},
               f4 = {f.dl, f.ul}, s1 = {s.ul, s.ur}, s2 = {s.ur, s.dr},
               s3 = {s.dr, s.dl}, s4 = {s.dl, s.ul};

    for (const Line *p1 : {&f1, &f2, &f3, &f4})
        for (const Line *p2 : {&s1, &s2, &s3, &s4})
            if (linesIntersect(p1, p2))
                return true;
    return false;
}

bool checkMoveRectCollision(const SDL_Rect &prev, const CollideC &curr, Rect &f,
                            Rect s) {
    makeMoveRect_rotated(prev, curr.rect, s);
    return rectsIntersect(f, s);
}

} // namespace collision
