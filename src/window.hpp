#pragma once
#include "game.hpp"
#include <SDL.h>

class Window {
  public:
    Window(const std::string basePath, const std::string prefPath);
    ~Window();
    void loop();

  private:
    void render();
    bool events();
    Game *game;
    SDL_Renderer *renderer;
    SDL_Window *window;
    SDL_Event event;
    const std::string basePath, prefPath;
};
