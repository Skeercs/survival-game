#include "window.hpp"

Window::Window(const std::string basePath, const std::string prefPath)
    : basePath(basePath), prefPath(prefPath) {
    window = SDL_CreateWindow("Game", SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, 1920, 1080,
                              SDL_WINDOW_RESIZABLE | SDL_WINDOW_FULLSCREEN);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);

    if (window == NULL) {
        std::cout << "Window error: " << SDL_GetError() << std::endl;
        exit(1);
    }
    if (renderer == NULL) {
        std::cout << "Renderer error: " << SDL_GetError() << std::endl;
        exit(1);
    }
    game = new Game(renderer, window, basePath, prefPath);
}

Window::~Window() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    delete game;
}

void Window::render() { game->draw(); }

bool Window::events() {
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        case SDL_QUIT:
            return 1;
            break;
        default:
            game->handleEvent(event);
        }
    }
    return 0;
}

void Window::loop() {
    Uint32 lastTime, currentTime = SDL_GetTicks();
    float deltaTime;
    while (1) {
        render();
        SDL_RenderPresent(renderer);
        lastTime = currentTime;
        currentTime = SDL_GetTicks();
        if (currentTime >= lastTime)
            deltaTime = (currentTime - lastTime) * 0.001;
        if (events())
            return;
        game->update(deltaTime, currentTime / 1000);
    }
}
