#include "window.hpp"
#include <SDL_image.h>

int main() {
    srand(time(nullptr));

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO)) {
        std::cout << "SDL_Init error: " << SDL_GetError() << std::endl;
        return 1;
    }

    if (!IMG_Init(IMG_INIT_PNG)) {
        std::cout << "IMG_Init error: " << IMG_GetError() << std::endl;
        return 1;
    }

    SDL_free(SDL_GetPrefPath(nullptr, "survival/save"));
    char *basePath = SDL_GetBasePath(),
         *prefPath = SDL_GetPrefPath(nullptr, "survival");
    Window window(basePath, prefPath);
    SDL_free(basePath);
    SDL_free(prefPath);

    window.loop();

    IMG_Quit();
    SDL_Quit();
}
