CXXFLAGS := `sdl2-config --cflags`
LDFLAGS := `sdl2-config --libs && pkg-config --libs SDL2_image`
WARNING := -Wall -Wextra

OBJDIR := obj
SRCDIR := src
DEPDIR := dep

SOURCES := $(wildcard $(SRCDIR)/*.cpp)
OBJECTS := $(patsubst $(SRCDIR)/%.cpp,$(OBJDIR)/%.o,$(SOURCES))
DEPENDS := $(patsubst $(SRCDIR)/%.cpp,$(DEPDIR)/%.d,$(SOURCES))

.PHONY: clean compile

game: $(OBJECTS)
	$(CXX) $(WARNING) $(LDFLAGS) $^ -o $@ -g

clean:
	$(RM) $(OBJECTS) $(DEPENDS) game
compile:
	$(CXX) $(SOURCES) -o game $(LDFLAGS) $(CXXFLAGS) -O2 -s

-include $(DEPENDS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp Makefile
	@mkdir -p $(@D)
	@mkdir -p $(DEPDIR)
	$(CXX) $(WARNING) $(CXXFLAGS) -c $< -o $@ -g -MMD -MP -MF $(patsubst $(SRCDIR)/%.cpp,$(DEPDIR)/%.d,$<) $(LDFLAGS)
