# Survival game

Survival game made in C++ and [SDL2](https://www.libsdl.org "Simple DirectMedia Layer")

## Pre-requisites

SDL2 and SDL2_image

## Compile and run (Linux)

`make compile`

`./game`

## License

[GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
